
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var grafPodatki = [0.0, 0.0, 0.0, 0.0, 0.0];
var grafPodatkiMin = [0.0, 0.0, 0.0, 0.0, 0.0];
var grafPodatkiMax = [0.0, 0.0, 0.0, 0.0, 0.0];
var podatkiOPacientu = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0];

var gnrIme = ["Jože", "Micka", "Lovrenc"];
var gnrPriimek = ["Gorišek", "Kovač", "Kuhar"];
var gnrRojstvo = ["1957-03-10T09:08", "1958-03-10T09:08", "1959-03-10T09:08"];
var gnrId = ["", "", ""];

var gnrVis = [180.0, 170.0, 170.0];
var gnrTez = [80.0, 60.0, 120.0];
var gnrTem = [36.8, 39.8, 34.0];
var gnrSis = [110.0, 115.0, 135.0];
var gnrDia = [70.0, 75.0, 90.0];
var gnrOxy = [98.0, 90.0, 50.0];

var Graf;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	var i = stPacienta;
	$("#kreirajIme").val(gnrIme[i]);
	$("#kreirajPriimek").val(gnrPriimek[i]);
	$("#kreirajDatumRojstva").val(gnrRojstvo[i]);
	kreirajEHRzaBolnika(stPacienta);
	
	return $("#preberiEHRid").val();
}
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function generirajPodatke2(stPacienta) {
	var i = stPacienta;
	gnrId[i] = $("#preberiEHRid").val();
	
	$("#dodajVitalnoEHR").val(gnrId[i]);
	$("#dodajVitalnoTelesnaVisina").val(gnrVis[i]);
	$("#dodajVitalnoTelesnaTeza").val(gnrTez[i]);
	$("#dodajVitalnoTelesnaTemperatura").val(gnrTem[i]);
	$("#dodajVitalnoKrvniTlakSistolicni").val(gnrSis[i]);
	$("#dodajVitalnoKrvniTlakDiastolicni").val(gnrDia[i]);
	$("#dodajVitalnoNasicenostKrviSKisikom").val(gnrOxy[i]);
	
	$("#meritveVitalnihZnakovEHRid").val(gnrId[i]);
	dodajMeritveVitalnihZnakov();

	document.getElementById('preberiEhrIdZaVitalneZnake').innerHTML += "<option value=" + gnrId[i] + ">" + gnrIme[i] + " " + gnrPriimek[i] + "</option>";
}

/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika(stPacienta) {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                    $("#dodajVitalnoEHR").val(ehrId);
		                    $("#meritveVitalnihZnakovEHRid").val(ehrId);
		                    if (stPacienta > -1){
		                    	generirajPodatke2(stPacienta);
		                    }
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": "2080-05-08T11:40Z",
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',

		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
    
    var results = "";
    
	if (!ehrId || ehrId.trim().length == 0) {                          
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
                                                        "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    	    
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                                "podatkov za bolnika <b>'" + party.firstNames +
                                    " " + party.lastNames + "'</b>.</span><br/><br/>");
                
                results = "<table class='table table-striped " +
                                "table-hover'><tr><th>Merjena količina</th>" +
                                "<th class='text-right'>Vrednost količine</th></tr>";
                
                
                //preberimo vse podatlke o visini, tezi, tlaku....

                $.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "height",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
 
				            results += "<tr><td>" + "Telesna višina" +
                                        "</td><td class='text-right'>" + res[0].height +
                                        " " + res[0].unit + "</td></tr>";
                            podatkiOPacientu[0] = res[0].height;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                                "<span class='obvestilo label label-warning fade-in'>" +
                                                    "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
                $.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + "weight",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {

				            results += "<tr><td>" + "Telesna teža" +
                                            "</td><td class='text-right'>" + res[0].weight + " " +
                                            res[0].unit + "</td></tr>";
                            podatkiOPacientu[1] = res[0].weight;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                        "<span class='obvestilo label label-warning fade-in'>" +
                                            "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
				$.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
 
				            results += "<tr><td>" + "Telesna temperatura" +
                                        "</td><td class='text-right'>" + res[0].temperature +
                                        " " + res[0].unit + "</td></tr>";
                            podatkiOPacientu[2] = res[0].temperature;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                                "<span class='obvestilo label label-warning fade-in'>" +
                                                    "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
				$.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
 
				            results += "<tr><td>" + "Sistolični krvni tlak" +
                                        "</td><td class='text-right'>" + res[0].systolic +
                                        " " + res[0].unit + "</td></tr>";
                            podatkiOPacientu[3] = res[0].systolic;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                                "<span class='obvestilo label label-warning fade-in'>" +
                                                    "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
				$.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {

				            results += "<tr><td>" + "Diastolični krvni tlak" +
                                        "</td><td class='text-right'>" + res[0].diastolic +
                                        " " + res[0].unit + "</td></tr>";
                            podatkiOPacientu[4] = res[0].diastolic;
                            results += "</table>";
		                    $("#rezultatMeritveVitalnihZnakov").append(results);
		                    narisiGraf();
		                    izracunajTriazo();
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                                "<span class='obvestilo label label-warning fade-in'>" +
                                                    "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
			    $.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "spO2",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
				    	    
				            results += "<tr><td>" + "Nasičenost krvi s kisikom" +
                                        "</td><td class='text-right'>" + res[0].spO2 +
                                        "%</td></tr>";
                            podatkiOPacientu[5] = res[0].spO2;
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                                "<span class='obvestilo label label-warning fade-in'>" +
                                                    "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function narisiGraf() {
	
	var stevila = beriTextFile("vir.txt");
	
	for (var i = 0; i < stevila.length/2; i++){
		grafPodatkiMin[i] = parseFloat(stevila[2*i]);
		grafPodatkiMax[i] = parseFloat(stevila[2*i+1]);
	}
	
    grafPodatki = [0.0, podatkiOPacientu[2], podatkiOPacientu[3], podatkiOPacientu[4], podatkiOPacientu[5]];
    var bmi = podatkiOPacientu[1] / ((podatkiOPacientu[0] / 100.0) * (podatkiOPacientu[0] / 100.0));
    bmi = bmi.toFixed(2);
	grafPodatki[0] = bmi;
    
    for (var member in Graf) delete Graf[member];
    
    var chrt = document.getElementById("mycanvas").getContext("2d");
        var data = {
            labels: ["BMI [kg/m^2]", "Temperatura [°C]", "Sistolični krvni tlak [mm Hg]", "Diastolični tlak [mm Hg]", "Nasičenost z O2"],
            datasets: [
                {
                    label: "Spodnja Meja",
                    fillColor:          "rgba(120,120,120,0.8)",
                    strokeColor:        "rgba(120,120,120,0.8)",
                    highlightFill:      "rgba(150,150,150,0.75)",
                    highlightStroke:    "rgba(150,150,150,1)",
                    data: grafPodatkiMin
                },
        		{
                    label: "Meritve",
                    fillColor:          "rgba(220,120,220,0.8)",
                    strokeColor:        "rgba(220,120,220,0.8)",
                    highlightFill:      "rgba(250,150,250,0.75)",
                    highlightStroke:    "rgba(250,150,250,1)",
                    data: grafPodatki
                },
                {
                    label: "Zgornja Meja",
                    fillColor:          "rgba(190,190,190,0.8)",
                    strokeColor:        "rgba(190,190,190,0.8)",
                    highlightFill:      "rgba(220,220,220,0.75)",
                    highlightStroke:    "rgba(220,220,220,1)",
                    data: grafPodatkiMax
                }
            ]
        };
    Graf = new Chart(chrt).Bar(data);
}
function izracunajTriazo(){
    var severity = 0;
    for (var i = 0; i < grafPodatki.length; i++) {
        if ((grafPodatki[i] < grafPodatkiMin[i]) || (grafPodatki[i] > grafPodatkiMax[i])){
            severity = severity + 1;
        }
    }
    if (severity>=4) {
        $("#barva").html("Vaša barva: <span style='color:black'> Črna</span>");
        document.body.style.backgroundColor="#C0C0C0";
        document.getElementById('crn').click();
    } else if(severity >=3){
        $("#barva").html("Vaša barva: <span style='color:red'> Rdeča</span>");
        document.body.style.backgroundColor="#FFCCCC";
        document.getElementById('rdec').click();
    } else if(severity >=2){
        $("#barva").html("Vaša barva: <span style='color:#CCCC00'> Rumena</span>");
        document.body.style.backgroundColor="#FFFFCC";
        document.getElementById('rumen').click();
    } else if(severity >=1){
        $("#barva").html("Vaša barva: <span style='color:green'> Zelena</span>");
        document.body.style.backgroundColor="#CCFFCC";
        document.getElementById('zelen').click();
    } else {
        $("#barva").html("Vaša barva: <span style='color:grey'> Bela</span>");
        document.body.style.backgroundColor="#FFFFFF";
        document.getElementById('bel').click();
    }
}
function beriTextFile(file){
	var stevila = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
	
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                var regexp = /[-]{0,1}[\d.]*[\d]+/g;
				stevila = allText.match(regexp);
            }
        }
    }
    rawFile.send(null);
    return stevila;
}

function generirajPodatkeHtml(){
	document.getElementById('gumb_generiraj').click();
}
function izprazniVnosnaPolja(){
	$("#kreirajIme").val("");
	$("#kreirajPriimek").val("");
	$("#kreirajDatumRojstva").val("");
	$("#dodajVitalnoEHR").val("");
	$("#dodajVitalnoTelesnaVisina").val("");
	$("#dodajVitalnoTelesnaTeza").val("");
	$("#dodajVitalnoTelesnaTemperatura").val("");
	$("#dodajVitalnoKrvniTlakSistolicni").val("");
	$("#dodajVitalnoKrvniTlakDiastolicni").val("");
	$("#dodajVitalnoNasicenostKrviSKisikom").val("");
	$("#meritveVitalnihZnakovEHRid").val("");
	$("#preberiEHRid").val("");
	$("#ivpSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno izpraznjeno.</span>");
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[1]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[2]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[3]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[5]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[6]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
	
	document.getElementById('bel').onclick = function(){
		document.getElementById('triaza_vsebina').innerHTML = "<p><b>Kategorija:</b> /</p><p><b>Opis: </b> Ste popolnoma zdravi :)</p>";
	};
	document.getElementById('zelen').onclick = function(){
		document.getElementById('triaza_vsebina').innerHTML = "<p><b>Kategorija:</b> III. (zelena)</p><p><b>Opis: </b>Tretja prioriteta. Lokalizirane poškodbe brez hitrih sistemskih učinkov. Poškodovanci lahko počakajo na oskrbo ali prevoz več ur. Primeri: nezavestni, ki spontano dihajo in nimajo znakov povečanega znotrajlobanjskega pritiska (reaktivne zenice, reakcija umika na bolečinski dražljaj, normalno dihanje), poškodovanci s stabilnim krvnim obtokom, s poškodbami posameznega uda.</p>";
	};
	document.getElementById('rumen').onclick = function(){
		document.getElementById('triaza_vsebina').innerHTML = "<p><b>Kategorija:</b> II. (rumena)</p><p><b>Opis: </b>Druga prioriteta, nujni poškodovanci. Poškodbe lahko ogrozijo življenje, toda trenutno je dihanje zadovoljivo, poškodovanec ni šokiran. Ob ustrezni prvi pomoči lahko počaka do eno uro brez večjega tveganja. Primeri: zlomi več udov, poškodbe prsnega koša in trebuha s stabilnim krvnim obtokom, velike krvaveče rane pri malo prizadetih poškodovancih.";
	};
	document.getElementById('rdec').onclick = function(){
		document.getElementById('triaza_vsebina').innerHTML = "<p><b>Kategorija:</b> I. (rdeča)</p><p><b>Opis: </b>Prva prioriteta, največja nujnost. Poškodovani je življenjsko ogrožen zaradi hipoksije ali šoka, vendar je narava bolezni ali poškodbe takšna, da ga lahko ob ustrezni in pravočasni oskrbi rešimo. Primeri: poškodovanci s hudimi poškodbami prsnega koša in trebuha ali udov, s hujšimi zunanjim/notranjimi krvavitvami, obsežnimi opeklinami. Prepoznamo jih po hitrem in plitvem dihanju, bledici, hitrem in šibkem pulzu.</p>";
	};
	document.getElementById('crn').onclick = function(){
		document.getElementById('triaza_vsebina').innerHTML = "<p><b>Kategorija:</b> IV. (črna)</p><p><b>Opis: </b>Mrtvi. V množičnih nesrečah ne smemo razlikovati klinične od biološke smrti. Vse poškodovance, ki ne dihajo sami ali so klinično brez srčne akcije, proglasimo za mrtve. Sem uvrščamo tudi poškodovance z obsežnimi hudimi poškodbami, ki imajo tudi ob optimalni medicinski oskrbi slabe možnosti za preživetje (amputacije več udov, obsežne opekline, amputacije delov trupa, izpadi notranjih organov skozi obsežne rane na trebuhu ali prsnem košu). Tem poškodovancem nudimo protibolečinsko terapijo, ne tratimo pa časa in zmogljivosti na račun oskrbe drugih bolj perspektivnih poškodovancev.</p>";
	};
	
	//umetno prozimo kreiranje
	document.getElementById('gumb_generiraj').onclick = function(){
		$('#preberiEhrIdZaVitalneZnake').html("<option value='" + "'></option>");
		for(var j = 0; j < 3; j++){
			generirajPodatke(j);
		}
		$("#gphSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno zgenerirano.</span>");
	};
});


